![LinuxyDEV Logo](https://gitlab.com/linuxy-dev/website/raw/master/assist/assists/LInuxyDEV_LOGO_BLUE.png)


### Our Frontend website for [LinuxyDEV.net](https://www.linuxydev.net)

Our Website is made with the following...

Languages: HTML, CSS, JS

If you like to contribute, Please open a issue or email us.

Contact Us: contact@linuxydev.net 
